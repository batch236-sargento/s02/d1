package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class S2A2 {
    public static void main(String[] args){

        int[] primeNumber = new int[5];

        primeNumber[0] = 2;
        primeNumber[1] = 3;
        primeNumber[2] = 5;
        primeNumber[3] = 7;
        primeNumber[4] = 11;

        System.out.println("The first prime number is: " + primeNumber[0]);
        System.out.println("The second prime number is: " + primeNumber[1]);
        System.out.println("The third prime number is: " + primeNumber[2]);
        System.out.println("The fourth prime number is: " + primeNumber[3]);
        System.out.println("The fifth prime number is: " + primeNumber[4]);


        ArrayList<String> friends = new ArrayList<String>(Arrays.asList());
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        System.out.println("My friends are: " + friends);

        HashMap<String, Integer> products = new HashMap<String, Integer>();
        products.put("toothpaste", 15);
        products.put("toothbrush", 20);
        products.put("soap", 12);
        System.out.println("Our current inventory consist of : " + products);


    }
}
